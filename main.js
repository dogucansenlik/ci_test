var express = require('express');
var app = express();
var port = process.env.PORT || 3001;

var server = app.listen(port, function () {
  console.log("Example app listening at %s http://%s:%s", global.environment, host, port)
});

module.exports = server;
